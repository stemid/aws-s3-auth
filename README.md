# Protect an AWS S3 bucket with basic HTTP authentication

Rewrite of [this repo](https://github.com/builtinnya/aws-lambda-edge-basic-auth-terraform).

This modules takes as input username, password, and a bucket resource. It then creates a lambda with a simple Javascript function that requests username and password from visitors, a Cloudfront distribution to serve it, and hides the S3 bucket content behind this authentication.

Return value is the actual URL you can call to access the bucket.

I personally use this to serve Ignition configs to CoreOS over HTTPS, instead of stuffing them all into the user data field.

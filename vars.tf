variable "function_name" {
  type = string
  default = "basicAuth"
  description = "Name of lambda function"
}

variable "basic_auth_credentials" {
  type = map
  description = "Credentials for Basic Authentication. Pass a map composed of 'user' and 'password'."
}

variable "bucket" {
  type = string
  description = "Bucket name"
}

variable "geo_restriction" {
  type = object({
    restriction_type = optional(string, "none")
    locations = optional(list(string), [])
  })
  default = {}
  description = "Geo restrictions in the form of a list of ISO3166 Alpha-2 country codes"
}

"use strict";

exports.handler = function(event, context, callback) {
  var s = event.Records[0].cf.request
  var i = s.headers
  var n = new Buffer(
    "".concat("${username}",":")
    .concat("${password}")
  ).toString("base64")
  var o = "Basic ".concat(n)
  if (void 0 !== i.authorization && i.authorization[0].value == o) {
    callback(null, s)
  } else {
    callback(null, {
      status: "401",
      statusDescription: "Unauthorized",
      body: "Unauthorized",
      headers: {
        "www-authenticate": [
          { key:"WWW-Authenticate", value:"Basic"}
        ]
      }
    })
  }
};

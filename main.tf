# See: https://docs.aws.amazon.com/AmazonCloudFront/latest/DeveloperGuide/lambda-edge-permissions.html
data "aws_iam_policy_document" "assumerole" {
  statement {
    actions = ["sts:AssumeRole"]
    effect = "Allow"
    principals {
      type = "Service"
      identifiers = [
        "lambda.amazonaws.com",
        "edgelambda.amazonaws.com"
      ]
    }
  }
}

resource "aws_iam_role" "lambda" {
  assume_role_policy = data.aws_iam_policy_document.assumerole.json
}

data "aws_iam_policy_document" "logs" {
  statement {
    sid = "all"
    effect = "Allow"
    actions = [
      "logs:CreateLogGroup",
      "logs:CreateLogStream",
      "logs:PutLogEvents"
    ]
    resources = [
      "arn:aws:logs:*:*:*"
    ]
  }
}

resource "aws_iam_role_policy" "lambda" {
  depends_on = [
    aws_iam_role.lambda
  ]
  role = aws_iam_role.lambda.id
  policy = data.aws_iam_policy_document.logs.json
}

data "archive_file" "basic_auth_function" {
  type = "zip"
  output_path = "${path.module}/templates/basic-auth.zip"

  source {
    content = templatefile(
      "${path.module}/templates/basic-auth.js",
      var.basic_auth_credentials
    )
    filename = "basic-auth.js"
  }
}

resource "aws_lambda_function" "basic_auth" {
  depends_on = [
    aws_iam_role.lambda,
  ]

  filename = "${path.module}/templates/basic-auth.zip"
  function_name = var.function_name
  role = aws_iam_role.lambda.arn
  handler = "basic-auth.handler"
  source_code_hash = data.archive_file.basic_auth_function.output_base64sha256
  runtime = "nodejs18.x"
  description = "Protect CloudFront distributions with Basic Authentication"
  publish = true

  provider = aws.us-east-1
}

data "aws_s3_bucket" "private" {
  bucket = var.bucket
}

data "aws_iam_policy_document" "bucket_policy" {
  statement {
    principals {
      type = "AWS"
      identifiers = [aws_cloudfront_origin_access_identity.private.iam_arn]
    }

    actions = [
      "s3:GetObject",
    ]

    resources = [
      data.aws_s3_bucket.private.arn,
      "${data.aws_s3_bucket.private.arn}/*"
    ]
  }
}

resource "aws_s3_bucket_policy" "cloudfront_policy" {
  bucket = data.aws_s3_bucket.private.id
  policy = data.aws_iam_policy_document.bucket_policy.json
}

resource "aws_s3_object" "index" {
  bucket       = data.aws_s3_bucket.private.id
  key          = "index.html"
  source       = "${path.module}/templates/index.html"
  content_type = "text/html"
  etag         = filemd5("${path.module}/templates/index.html")
}

resource "aws_cloudfront_origin_access_identity" "private" {}

resource "aws_cloudfront_distribution" "private" {
  depends_on = [
    aws_cloudfront_origin_access_identity.private,
    aws_lambda_function.basic_auth
  ]

  origin {
    domain_name = data.aws_s3_bucket.private.bucket_regional_domain_name
    origin_id = "S3-${data.aws_s3_bucket.private.id}"

    s3_origin_config {
      origin_access_identity = aws_cloudfront_origin_access_identity.private.cloudfront_access_identity_path
    }
  }

  enabled             = true
  is_ipv6_enabled     = true
  comment             = "HTTP Basic auth protected S3 bucket"
  default_root_object = "index.html"

  default_cache_behavior {
    allowed_methods        = ["GET", "HEAD"]
    cached_methods         = ["GET", "HEAD"]
    target_origin_id       = "S3-${data.aws_s3_bucket.private.id}"
    viewer_protocol_policy = "redirect-to-https"
    min_ttl                = 0
    default_ttl            = 3600
    max_ttl                = 86400

    forwarded_values {
      query_string = true

      cookies {
        forward = "all"
      }
    }

    lambda_function_association {
      event_type   = "viewer-request"
      lambda_arn   = aws_lambda_function.basic_auth.qualified_arn
      include_body = false
    }
  }

  viewer_certificate {
    cloudfront_default_certificate = true
  }

  restrictions {
    geo_restriction {
      # Set to "none" and remove locations if you want no resitrctions
      restriction_type = var.geo_restriction.restriction_type
      locations = var.geo_restriction.locations
    }
  }

  provider = aws.us-east-1
}

